# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

with open('README.rst') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='parameter difference report',
    version='0.1.0',
    description='Script to generate parameter differences between two SQL databases',
    long_description=readme,
    author='Quinlin Riggs',
    author_email='quinlin.riggs@thermofisher.com',
    url='https://bitbucket.org/riggsq/parameterdifferencereport/src',
    license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)