# Helper functions to write to xlsx files

import openpyxl
import numpy as np
import pandas as pd

from pathlib import Path
from openpyxl import load_workbook


def write_to_xl(excel_path: Path, data: pd.DataFrame, param_dict: dict) -> None:
    """Generates a .xlsx file at the input path and writes data specified by
    data input. By default writes each entry in data to a new sheet.

    Args:
        excel_path: path to excel sheet. Will be create if does not exist
        data      : list of tuples containing [(pandas.DataFrame, 'SheetName')]
        param_dict: python dict containing
                    {sql_table_name: [(param1), (param2), ...], ... }

    Returns:
        None

    """

    def highlight_diff(data, color='#5fba7d'):
        """Helper to highligh excel cells"""
        diff_color = 'background-color: {}'.format(color)
        #org_color  = 'background-color: {}'.format('#ba5f9c')
        other = data.xs('First', axis='columns', level=-1)
        # condition is pd.notequal compare, return either diff color or none
        return pd.DataFrame(np.where(data.ne(other, level=0), diff_color, ''),
                            index=data.index, columns=data.columns)

    if not excel_path.exists():
        wb = openpyxl.Workbook()
        try:
            wb.save(excel_path)
        except PermissionError:
            # Excel file is already open, don't overwrite
            return False

    writer = pd.ExcelWriter(excel_path, engine = 'openpyxl')
    book = load_workbook(excel_path)
    writer.book = book
    writer.sheets = dict((ws.title, ws) for ws in book.worksheets)

    # Remove inital sheet if present
    if 'Sheet' in writer.book:
        writer.book.remove(writer.book['Sheet'])
    if 'Sheet1' in writer.book:
        writer.book.remove(writer.book['Sheet1'])

    # d[0] -> pandas df
    for i, d in enumerate(data):

        columns = [x[1] for x in param_dict[d[1]]]
     
        if not d[0].empty: # avoid calling empty 

            # d[0] has been concated with a first/second layer
            # switch them to sort based on lower level (compare first/second)
            d[0].columns = d[0].columns.swaplevel(1, 0)
            d[0].sort_index(axis=1, level=0, inplace=True)

            # Enforce column order
            d[0] = d[0][columns]
            # Highlight differences
            d[0] = d[0].style.apply(highlight_diff, axis=None)

        d[0].to_excel(writer, sheet_name = d[1])
        try:
            writer.save()
            writer.close()
        except PermissionError:
            # Excel file is already open, don't overwrite
            return False
    return True


