# Helper functions to record data sql databases

import sqlite3

from pathlib import Path


def get_sql_parameters(sql_path: Path) -> dict:
    """ Returns a dictionary of all sql tables and thier parameters as a
    list of tuples

    Args:
        sql_path: pathlib Path to decrypted sql database

    Returns:
        db_dict : dict {sql_table1 : [(param1*), (param2), ...], 
                        sql_table2 : [(param1*) ... }

    """

    db_dict = {}

    with sqlite3.connect(sql_path) as db:
        cursor = db.cursor()
        # sqlite_master is a special sqlite3 table that contains all tables in db
        cursor.execute("""SELECT name 
                          FROM sqlite_master 
                          WHERE type='table';""")
        tables = cursor.fetchall()

        for table_name in tables:
            table_name = table_name[0]  # table returned in a tuple
            db_dict[table_name] = []
            # List all parameteres and append
            for row in db.execute("pragma table_info(%s)" % table_name).fetchall():
                db_dict[table_name].append(row)

    return db_dict


def get_sql_diff(control_path: Path, check_path: Path) -> None:
    """Return dict containing sub dicts for each table. Dict values are a list
    containing two sql results. SQL results are the values that differ from the
    other input sql, i.e. the control sql return will contain control sql values
    that are different in the check path and check sql return will return sql 
    values that are different in the control path.

    Args:
        control_path: pathlib Path to decrypted control sql database
        check_path  : pathlib Path to decrypted check sql database

    Returns:
        db_dict     : python dict with the structure
                      {sql_table_name: [sql_return_control, sql_return_check], ... }

    """

    db_dict = {}

    with sqlite3.connect(control_path) as db1:
        
        cursor = db1.cursor()
       
        cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")
        tables = cursor.fetchall()

        db1.execute("ATTACH ? AS db2", [str(check_path)])

        for table_name in tables:

            table_name = table_name[0]

            # SQL Call to compare differences in main and attached db

            #call = ("""select * from (select * from main.{0} except select * from db2.{1}) as T
            #           union all
            #           select * from (select * from db2.{2} except select * from main.{3}) as T"""
            #           .format(table_name, table_name, table_name, table_name))

            # Break into two seperate dict to distinguish between new and old
            res1 = db1.execute("""SELECT * 
                                  FROM (SELECT * 
                                        FROM main.{0} 
                                        EXCEPT SELECT * 
                                               FROM db2.{1}) as T
                                """.format(table_name, table_name)).fetchall()
            res2 = db1.execute("""SELECT * 
                                  FROM (SELECT * 
                                        FROM db2.{0} 
                                        EXCEPT SELECT * 
                                               FROM main.{1}) as T
                                """.format(table_name, table_name)).fetchall()

            db_dict[table_name] = [res1, res2]

    return db_dict


def add_parameters_to_sql(sql_path: Path, parameter_dict: tuple) -> None:
    """ Modify a sql database by adding parameters specified by input dict

    Args:
        sql_path      : pathlib Path to decrypted sql database to be updated
        parameter_dict: dict in the form of {sql_table1 : [(param1*), (param2), ...], 
                                             sql_table2 : [(param1*) ... }
                        where each param will be added to the input sql at the
                        associated sql_table key

    Returns:
        None

    """

    with sqlite3.connect(sql_path) as db:
        cursor = db.cursor()
 
        for table in parameter_dict:
            # If data
            if parameter_dict[table]:
                # Each entry is a unique parameter to add
                for parameter in parameter_dict[table]:
                    # table is a single tuple, index
                    cursor.execute("ALTER TABLE {0} ADD COLUMN '{1}test' {2}".format(table[0], parameter[1], parameter[2]))
                    db.commit()


def test_connection(db_path: Path) -> bool:
    """Test a database connection to a SQLite database 
    
    Args:
        db_path: path to database

    Returns
        bool   : true if able to connect otherwise false

    """
    try:
        conn = sqlite3.connect(str(db_path))

        if conn is not None:
            # check encrypted
            cursor = conn.cursor()
            try:
                cursor.execute("""SELECT name 
                                  FROM sqlite_master 
                                  WHERE type='table';""")
            except sqlite3.DatabaseError:
                # Encrypted/ not a real table
                conn.close()
                return False
            
            conn.close()
            return True

        return False

    except Error as err:
        return False