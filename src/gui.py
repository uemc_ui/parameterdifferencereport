#!/usr/bin/env python3

# 11/7/19 Quinlin Riggs

import sys
import PyQt5

from PyQt5.QtWidgets import QApplication

from gui_app_class import App


if __name__=='__main__':
    app = QApplication(sys.argv)
    main_app = App(app)
    sys.exit(app.exec_())
