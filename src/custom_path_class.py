""" Quinlin Riggs """

import os
import subprocess

from pathlib import Path

class ThermoPath:

    """Generic path helper object"""

    def __init__(self, top):
        self._top = self._return_path(top)
        self._parent = self._update_path_parent()
        self._encrpt_pass = '"ThermoFisherDeltaK-Network-Auth-Key"'

    @property
    def top(self) -> Path:
        """Property definition"""
        return self._top

    @property
    def parent(self) -> Path:
        """Property definition"""
        return self._parent

    @top.setter
    def top(self, in_path: Path):
        self._top = self._return_path(in_path)
        self._update_path_parent()

    def _return_path(self, in_path: str) -> Path:
        """Verify top path, try and resolve if not absolute"""
        if Path(in_path).is_absolute() and Path(in_path).exists():
            return Path(in_path)
        if Path(in_path).resolve(strict=True).exists():
            return Path(in_path).resolve(strict=True)
        raise FileNotFoundError('Unable to verify input path %s' % in_path)

    def _validate_path(self, in_path: Path) -> Path:
        """Bool compare to ensure external paths are absolute/exist"""
        if isinstance(in_path, Path):
            if in_path.is_absolute() and in_path.exists():
                return True
            return False
        return False

    def _update_path_parent(self) -> None:
        """Reset parent after search"""
        if self._top: # top will be none if not a valid path
            return self._top.parent
        return None

    def get_unique_file(self, key: list, idepth: int=1,
                        topdown: bool=True, required: bool=False) -> Path:
        """Return a single file path matching key, search for depth
        i = 1 means it will only search current directory when topdown=False
        """
        for i in range(idepth):

            if self._top.is_dir() and i == 0:
                self._parent = self._top

            if i > 0:
                self._parent = self._parent.parent

            for root, _, files in os.walk(self._parent, topdown=topdown):
                for file in files:
                    if file.endswith(key):
                        self._parent = self._update_path_parent()
                        return Path(os.path.join(root, file))

        # If file is required raise exception, otherwise pass none silently
        if required:
            raise FileNotFoundError('Unable to find %s' % key)
        return None

    def get_file_list(self, key: list, idepth: int=1,
                      topdown: bool=True) -> list:
        """Returns a list of files matching input key. Key may be a single
        str, a tuple of strs or a list of strs. Matches are assumed to be
        below top. Infinite depth.
        """
        return_list = []

        for i in range(idepth):

            # start at top if dir
            if self._top.is_dir() and i == 0:
                self._parent = self._top

            if i > 0:
                self._parent = self._parent.parent

            for root, _, files in os.walk(self._parent, topdown=topdown):
                for file in files:
                    if file.endswith(key) and file not in return_list:
                        return_list.append(Path(os.path.join(root, file)))

        return return_list

    def generate_filename(self, in_path: Path,
                          out_name: str, exists: bool=True) -> Path:
        """Replace file name with new name and return path"""

        # If in_path not a path object and should exist, try and resolve one
        if exists:
            if not self._validate_path(in_path):
                in_path = self._return_path(in_path)

            if in_path:
                if in_path.is_file():
                    return in_path.parent / (out_name
                                             + '.'
                                             + in_path.name.split('.')[-1]
                                             if not '.' in out_name
                                             else out_name)
        else:
            if in_path:
                return in_path.parent / (out_name
                                         + '.'
                                         + in_path.name.split('.')[-1]
                                         if not '.' in out_name
                                         else out_name)

        raise FileNotFoundError('Unable to verify input path %s' % in_path)

    def decrypt_path(self, in_path: Path) -> Path:
        """Decrpyt in file and return a path to the decrypted file"""

        # If in_path not a path object, try and resolve one
        if not self._validate_path(in_path):
            in_path = self._return_path(in_path)

        if in_path:
            if in_path.is_file():
                out_path = self.generate_filename(in_path,
                                                  (in_path.name.split('.')[0]
                                                   + '_decrypt'))

                if out_path.exists():
                    pass # already decrypted
                else:
                    call = ('openssl enc -aes-256-cbc -d \
                             -in %s -out %s -k %s' % (in_path, out_path,
                                                      self._encrpt_pass))

                    try:
                        subprocess.run(call, shell=True, check=True)
                    except OSError as err:
                        print(err)

                return out_path

        raise FileNotFoundError('Unable to verify input path %s' % in_path)
