# 11/5/19 Quinlin Riggs

# Qt App Class

from PyQt5 import QtCore

from gui_main_window_class import Window
from gui_worker_class import WorkerObject

class App(QtCore.QObject):
    """Application Class, set up thread connections"""

    signalStatus = QtCore.pyqtSignal(str)
    signalControl = QtCore.pyqtSignal(str)
    signalCheck = QtCore.pyqtSignal(str)
    signalName = QtCore.pyqtSignal(str)

    def __init__(self, parent=None):
        super(self.__class__, self).__init__(parent)

        # Create a gui object.
        self.gui = Window()

        # Create a new worker thread.
        self.createWorkerThread()

        # Make any cross object connections.
        self._connectSignals()

        self.gui.show()

    def _connectSignals(self):
        # Window Connections -> Worker Connections in createWorkerThread
        self.gui.button_control.clicked.connect(self.gui.updateControl)
        self.gui.button_check.clicked.connect(self.gui.updateCheck)
        self.gui.line_name.editingFinished.connect(self.gui.updateName)

        # Signal Connections, emits from WorkerObject -> update gui
        self.signalStatus.connect(self.gui.updateStatus)

        # Emit from -> connect to call function
        self.signalControl.connect(self.worker.updateControl)
        self.signalCheck.connect(self.worker.updateCheck)
        self.signalName.connect(self.worker.updateName)

        # App Connections
        #self.gui.button_cancel.clicked.connect(self.forceWorkerReset)
        self.parent().aboutToQuit.connect(self.forceWorkerQuit)

    def createWorkerThread(self):
        # Setup the worker object and the worker_thread.
        self.worker = WorkerObject(self.gui.control, 
                                   self.gui.check, 
                                   self.gui.name)
        self.worker_thread = QtCore.QThread()
        self.worker.moveToThread(self.worker_thread)
        self.worker_thread.start()

        # Gui to Worker signal
        self.gui.signalControl.connect(self.worker.updateControl)
        self.gui.signalCheck.connect(self.worker.updateCheck)
        self.gui.signalName.connect(self.worker.updateName)

        # Worker to Gui signals
        self.worker.signalStatus.connect(self.gui.updateStatus)

        # Connect gui buttons to worker functions
        self.gui.button_start.clicked.connect(self.worker.startWork)

    def forceWorkerReset(self):      
        if self.worker_thread.isRunning():
            print('Terminating thread.')
            self.worker_thread.terminate()

            print('Waiting for thread termination.')
            self.worker_thread.wait()

            self.signalStatus.emit('Worker killed. Recreating new Worker...')

            print('building new working object.')
            self.createWorkerThread()

    def forceWorkerQuit(self):
        if self.worker_thread.isRunning():
            self.worker_thread.terminate()
            self.worker_thread.wait()
