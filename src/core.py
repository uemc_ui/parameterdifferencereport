#!/usr/bin/env python3

# 11/7/19 Quinlin Riggs

import argparse

from pathlib import Path

from custom_path_class import ThermoPath
from xlsx_helpers import write_to_xl
from dataframe_helpers import generate_dataframe_list
from sql_helpers import get_sql_parameters, get_sql_diff, \
                        add_parameters_to_sql


def main(control=None, check=None, cmdline=True, decrypt=False, name=None):

    top = ThermoPath('.')

    if cmdline:
        parser = argparse.ArgumentParser()

        parser.add_argument('-co', '--control_parameter_table',
                            help='parameter table that will be checked against',
                            metavar='PARAMETERTABLE',
                            required=True,
                            type=str)

        parser.add_argument('-ch', '--check_parameter_table',
                            help='parameter table that will be checked',
                            metavar='PARAMETERTABLE',
                            required=True,
                            type=str)

        parser.add_argument('-name', '--outfile_name',
                            help='.xlsx outfile name',
                            metavar='FILENAME',
                            required=False,
                            type=str)

        parser.add_argument('-dco', '--control_parameter_table_with_decrpyt',
                            help='flag that control table that will be decrpyted',
                            metavar='BOOL',
                            required=False,
                            default=False,
                            type=bool)

        parser.add_argument('-dch', '--check_parameter_table_with_decrypt',
                            help='flag that check table that will be decrypted',
                            metavar='BOOL',
                            required=False,
                            default=False,
                            type=bool)

        default_file_name = 'parameter_differences.xlsx'

        args = parser.parse_args()

        if args.control_parameter_table_with_decrpyt:
            control_path = top.decrypt_path(Path(r'%s' % args.control_parameter_table))
        else:
            control_path = Path(r'%s' % args.control_parameter_table).resolve()

        if args.check_parameter_table_with_decrypt:
            check_path = top.decrypt_path(Path(r'%s' % args.check_parameter_table))
        else:
            check_path = Path(r'%s' % args.check_parameter_table).resolve()

    else:
         if decrypt:
             control_path = top.decrypt_path(Path(r'%s' % control))
             check_path = top.decrypt_path(Path(r'%s' % check))
         else:
             control_path = Path(r'%s' % control)
             check_path = Path(r'%s' % check)

    if control_path.exists():
        pass
    else:
        raise FileNotFoundError('Control Table not found.')
    if check_path.exists():
        pass  # TODO: refactor, no longer needed
    else:
        raise FileNotFoundError('Check Table not found.')

    # Excel out file and path, check for user file name and update
    file_name = 'parameter_differences.xlsx'
    excel_path = control_path.parent / file_name

    if cmdline:
        if args.outfile_name:
            excel_path = top.generate_filename(excel_path, 
                                               args.outfile_name, 
                                               exists = False)
    elif name:
        excel_path = top.generate_filename(excel_path, 
                                           name, 
                                           exists = False)

    # Get data dict and sql parameter/columns
    diff_dict = get_sql_diff(control_path, check_path)

    param_dict = get_sql_parameters(control_path)

    data = generate_dataframe_list(diff_dict, param_dict)

    return_dict = {'message': '', 'errors': ''}

    if write_to_xl(excel_path, data, param_dict):
        return_dict['message'] = excel_path
    else:
        return_dict['errors'] = ('Permission Error, close %s and retry' 
                                 % excel_path.name)

    return return_dict


if __name__ == '__main__':
    main()
