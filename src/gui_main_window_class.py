# Qt Main Window Class

from PyQt5 import QtCore
from PyQt5.QtWidgets import QWidget, QPushButton, QLabel, QFileDialog, \
                            QFormLayout, QLineEdit, QGridLayout

from sql_helpers import test_connection


class Window(QWidget):
    """Main Window Widget"""

    # Custom signals
    signalControl = QtCore.pyqtSignal(str)
    signalCheck = QtCore.pyqtSignal(str)
    signalName = QtCore.pyqtSignal(str)

    def __init__(self):
        QWidget.__init__(self)

        self.setWindowTitle('Parameter Difference Report')

        #self.setStyleSheet("""background-color: #ecf0f1""")

        self.control = None
        self.check = None
        self.name = 'parameter_difference.xlsx'

        # Button Widgets
        self.button_control = QPushButton('Choose Control Table', self)
        self.label_control = QLineEdit()
        self.label_control.setReadOnly(True)
        self.label_control.setAlignment(QtCore.Qt.AlignLeft)

        self.button_check = QPushButton('Choose Check Table', self)
        self.label_check = QLineEdit()
        self.label_check.setReadOnly(True)
        self.label_check.setAlignment(QtCore.Qt.AlignLeft)

        self.line_name = QLineEdit(self.name)
        self.line_name.setAlignment(QtCore.Qt.AlignRight)
        
        self.button_start = QPushButton('Generate Report', self)
        #self.button_cancel = QPushButton('Cancel', self)
        self.label_status = QLabel('', self)
        self.label_status.setWordWrap(True)

        # Button Sub-Layout
        self.sub_layout = QFormLayout()
        self.sub_layout.addRow('', self.button_control)
        self.sub_layout.addRow('Control:', self.label_control)
        self.sub_layout.addRow('', self.button_check)
        self.sub_layout.addRow('Check:', self.label_check)
        self.sub_layout.addRow('Output Name:', self.line_name)

        self.sub_layout.addRow('', self.button_start)
        #self.sub_layout.addRow('', self.button_cancel)
        self.sub_layout.addRow('Status:', self.label_status)

        self.sub_layout.setGeometry(QtCore.QRect(0, 0, 900, 400))

        # Layout
        self.layout = QGridLayout()
        self.layout.addLayout(self.sub_layout, 0, 0)
        self.setLayout(self.layout)
        # screen x, y, width, height
        self.setGeometry(150, 150, 900, 400)

    # Slotted Functions, recieve emit from worker, etc
    @QtCore.pyqtSlot(str)
    def updateStatus(self, status):
        # Update Label
        self.label_status.setText(status)

    def updateControl(self):
        dialog = QFileDialog()
        # file name is first element of tuple
        self.control = dialog.getOpenFileName(self, 'Select Control Table', '.', 
                                              "Data Base File (*.db);; SQL (*.sql, *.sqlite)")[0]
        self.label_control.setText('{0}'.format(self.control))
        if self.control:
            if test_connection(self.control):
                self.updateStatus('')
                self.label_control.setStyleSheet("""QLineEdit{
                                                    background-color: #27ae60; 
                                                    color: white}""")
            else:
                self.updateStatus('Could not connect to %s' % self.control)
                self.control = None
                self.label_control.setStyleSheet("""QLineEdit{
                                                    background-color: #e74c3c; 
                                                    color: white}""")
            self.signalControl.emit(self.control)
        else:
            # if no file is selected, return box color to nominal
            self.label_control.setStyleSheet("""QLineEdit{
                                                background-color: white; 
                                                color: white}""")

    def updateCheck(self):
        dialog = QFileDialog()
        # file name is first element of tuple
        self.check = dialog.getOpenFileName(self, 'Select Check Table', '.', 
                                            "Data Base File (*.db);; SQL (*.sql, *.sqlite)")[0]
        self.label_check.setText('{0}'.format(self.check))
        if self.check:
            if test_connection(self.check):
                self.updateStatus('')
                self.label_check.setStyleSheet("""QLineEdit{
                                                  background-color: #27ae60; 
                                                  color: white}""")
            else:
                self.updateStatus('Could not connect to %s' % self.check)
                self.check = None
                self.label_check.setStyleSheet("""QLineEdit{
                                                  background-color: #e74c3c; 
                                                  color: white }""")
            self.signalCheck.emit(self.check)
        else:
            # if no file is selected, return box color to nominal
            self.label_check.setStyleSheet("""QLineEdit{
                                              background-color: white; 
                                              color: white }""")

    def updateName(self):
        self.name = self.line_name.text()
        self.signalName.emit(self.name)