# Helper functions to generate pandas dataframe objects from python objects

import pandas as pd


def generate_dataframe(db_dict: dict, orient: str = 'index', 
                       transpose: bool=True) -> pd.DataFrame:
    """Converts a pure python dict to a pandas dataframe object. Transpose
    allows for the keys of the dict to become column headers in an xlsx format

    Args:
        db_dict  : python dict
        transpose: bool, true then db_dict keys are column headers, 
                          false db_dict keys are the first element in each row

    Returns:
        df       : pandas dataframe

    """

    df = pd.DataFrame.from_dict(db_dict, orient=orient)

    if transpose:
        # Database tables is column header for parameters
        return df.transpose()
    else:
        return df


def generate_dataframe_list(db_dict: dict, param_dict: dict) -> list:
    """Converts a pure python dict to a list of pandas dataframe objects. 

    Args:
        db_dict   : python dict containing 
                    {sql_table_name: [sql_return_control, sql_return_check], ... }
        param_dict: python dict containing
                    {sql_table_name: [(param1), (param2), ...], ... }

    Returns:
        df_list  : list of concated pd.DataFrame objects created by concating
                   the two databases in the list of the value of the db_dict

    """

    df_list = []

    for key in db_dict:

        # Get columns from parameters
        columns = [x[1] for x in param_dict[key]]

        # Generate dataframes using parameters columns
        d1 = pd.DataFrame(db_dict[key][0], columns=columns)
        d2 = pd.DataFrame(db_dict[key][1], columns=columns)

        # Concatinate control/check values
        df_list.append([pd.DataFrame(pd.concat([d1, d2], 
                                               axis='columns', 
                                               keys=['First', 'Second'])), 
                        key])

    return df_list


