# Worker Class Object

import core

from PyQt5 import QtCore

class WorkerObject(QtCore.QObject):
    """Worker Object for new thread"""

    signalStatus = QtCore.pyqtSignal(str)

    def __init__(self, control, check, name, parent=None):
        super(self.__class__, self).__init__(parent)
        self.control = control
        self.check = check
        self.name = name

    @QtCore.pyqtSlot()        
    def startWork(self):
        # Start external scipt and return data from analysis
        if self.control and self.check:
            self.signalStatus.emit('Running...')
            run_dict = core.main(control=self.control, check=self.check,
                                 cmdline=False, decrypt=False, name=self.name)
            if run_dict['errors']:
                self.signalStatus.emit(run_dict['errors'])
            elif run_dict['message']:  # right now always a path
                short_path = '../' + run_dict['message'].parents[0].name + '/' + run_dict['message'].name
                self.signalStatus.emit('Difference Table written to %s' % short_path)
        else:
            self.signalStatus.emit('Input both Control and Check Table')

    @QtCore.pyqtSlot(str)
    def updateControl(self, input):
        self.control = input

    @QtCore.pyqtSlot(str)
    def updateCheck(self, input):
        self.check = input

    @QtCore.pyqtSlot(str)
    def updateName(self, input):
        self.name = input