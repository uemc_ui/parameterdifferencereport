Parameter Difference Report

gui usage --------------------------------------------------------------------

download /dist/ directory or clone repo
open and run /dist/ParameterDifference/ParameterDifference.exe

.. image:: gui_pic.png


command line setup & run------------------------------------------------------

unix:

pip3 install -r requirements.txt	
python3 src/core.py

windows:

pip install -r requirements.txt
python src/core.py

command line usage -----------------------------------------------------------

python core.py -co control_table -ch check_table [-n file_name] \ 
			   [-dco decrypt_control_table] [-dch decrypt_check_table]

-co   relative or absolute path to control table to be checked against
-ch   relative or absolute path to check table to check against control
-n    [opt] str, outfile name. Name of the output excel file, defaults to 
	  /dir/of/parameter/tables/parameter_difference.xlsx
-dco  [opt] bool True/False, decrpyt control table before comparison
-dch  [opt] bool True/False, decrpyt check table before comparison

sample usage:-----------------------------------------------------------------

with two parameter tables of interest in a sub folder called 'parameter_tables':
	1. HWID_03_HWREV_00_ParamTableDB.db            <---- Control Table
	2. HWID_03_HWREV_00_ParamTableDB_1.2.1.db	   <---- Check Table

run:
	python src/core.py -co parameter_tables/HWID_03_HWREV_00_ParamTableDB.db \
					   -ch parameter_tables/HWID_03_HWREV_00_ParamTableDB_1.2.1.db

to decrypt before comparison run:
	python src/core.py -co parameter_tables/HWID_03_HWREV_00_ParamTableDB.db -dco True \
					   -ch parameter_tables/HWID_03_HWREV_00_ParamTableDB_1.2.1.db -dch True

to set .xlsx name:
	python src/core.py -n my_file -co parameter_tables/HWID_03_HWREV_00_ParamTableDB.db \ 
					   -ch parameter_tables/HWID_03_HWREV_00_ParamTableDB_1.2.1.db

description:------------------------------------------------------------------

This script will compare two input SQL databases and check for parameter 
differences. It will write the differences a .xlsx file with the old and new 
values for the parameters that have been altered and will highlight the new
values in the the check table.